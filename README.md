# SEU/SET test software routines

##


```
This program creates 2 files :

* h5 file with the configuration and write, read matrices (192*400)
* tabular txt file with the seu-like events (in the current case, fake ones) with the row, col of the pixel and the write/read value.

```


## Instructions for installation
```
[Insert]
```


## Open the bashrc file
```
gedit ~/.bashrc
```

## Add this line bashrc: export PATH=$HOME/miniconda/bin:$PATH

```
export PATH=$HOME/miniconda/bin:$PATH
```




```
conda update --yes conda
conda install --yes numpy bitarray pytest pyyaml progressbar scipy numba pytables matplotlib tqdm
pip install cocotb
pip install pytemperature
pip install pyvisa
all the dependencies
```


## Install bdaq53

```
git clone -b [insert version] https://github.com/SiLab-Bonn/basil
cd basil

python setup.py develop
cd ..

git clone -b v0.9.0 https://gitlab.cern.ch/silab/bdaq53.git
cd bdaq53

python setup.py develop

git checkout -b development
```

## Configure your ip adress:

```
sudo ifconfig [insert eth. name] 192.168.10.2
```

## Test with ping if it works

```
ping 192.168.10.12
```


## Install mailx by following these guidelines
```
mailx instructions - https://gist.github.com/ilkereroglu/aa6c868153d1c5d57cd8
```
* [mailx](https://gist.github.com/ilkereroglu/aa6c868153d1c5d57cd8) 



## some other dependensies 
```
pip install psutil

pip install pyvisa-py

pip install pyusb
```




## Some useful Links
* [BDAQ53]( https://gitlab.cern.ch/silab/bdaq53) - Bdaq53 repo.
* [RD53A twiki](https://twiki.cern.ch/twiki/bin/viewauth/RD53/WebHome) - Info of RD53A




