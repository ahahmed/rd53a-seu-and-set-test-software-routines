#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic test writes random data into the specified registers,
    then reads back the data and compares read and written data.
'''
import sys

import random
import numpy as np
import time

np.set_printoptions(threshold=np.inf)

import tables as tb

from bdaq53.scan_base import ScanBase
from bdaq53.register_utils import RD53ARegisterParser
from bdaq53.analysis import analysis_utils

DEBUG = False   # Use option -v or -V or --verbose or -verbose to activate
                # Prints every tested register + associated value

### SYNCHRONOUS ###
sync = np.empty((192, 128), dtype=int)
#for configuration with every bit on 1, use the following
sync.fill(7)  # 7 for sync pixel
#for random values for each pixel, use the following (put more than 7 to simulate SEU-like errors)
for i in range(192):
    for j in range(128):
        sync[i,j]=random.randrange(14)

### LINEAR ###
lin = np.empty((192, 136), dtype=int)
#for configuration with every bit on 1, use the following
lin.fill(255)  # 255 for lin pixel
#for random values for each pixel, use the following
"""
for i in range(192):
    for j in range(136):
        lin[i,j]=random.randrange(255)
"""

### DIFFERENTIAL ###
diff = np.empty((192, 136), dtype=int)
#for configuration with every bit on 1, use the following
diff.fill(255)  # 255 for diff pixel
#for random values for each pixel, use the following
"""
for i in range(192):
    for j in range(136):
        diff[i,j]=random.randrange(255)
"""

### MERGING the 3 types of pixels ###
pixels_config = np.concatenate((sync, lin, diff),axis=1)
   
local_configuration = {
    # Scan parameters
    'pixels_config': pixels_config
    }


class SEUTable(tb.IsDescription):
    pixel_row = tb.UInt16Col()
    pixel_col = tb.UInt16Col()
    written_value = tb.UInt16Col()
    read_value = tb.UInt16Col()


class PixelTest(ScanBase):
    scan_id = 'pixel_test'
    
    def configure(self, **kwargs):
        super(PixelTest, self).configure(**kwargs)
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()

    def start(self, **kwargs):  # scans and analyzes are started here
        '''
            All steps of a complete scan implementation:
            - Initialize hardware (communication, periphery, ...)
            - Configure chip with std. settings
            - Configure chip with scan specific settings
            - Configure raw data handlng (file writing, data sending)
            - Add power supply measurement
            - Run scan routine
            - Add power supply measurement
            - Add end of scan info (link info, chip status)
        '''

        self.configuration['scan'] = kwargs
        # FIXME: Better usage of configuration object instead of kwargs!
        kwargs.update(self.configuration['chip'])
        kwargs.update(self.configuration['bench'])
        
        self.init(**kwargs)
        self.reset_chip()
        self._configure(**kwargs)  # Masks from default config
        self.configure(**kwargs)  # Configure step of scan

        kwargs['rws'] = range(192)  # which rows to write/readback. Name 'rows' is reserved
        active_column = 0  # first of currently tested columns
        step = 8  # number of columns to test together (at each spill). An even number n is as fast as n-1 (thanks to double column read/write)
        running = True
        finishing = False

        while running:  # loop to test every pixel according to column and row separation at each spill
            t1 = time.time() 
            self._configure_readout(**kwargs)  # Raw data handling
            
            self._add_powersupply_info()
            kwargs['cols'] = range(active_column, active_column+step)   # which columns to write/readback. Name 'columns' is reserved
                                                                        # 'step' columns are tested simultaneously 
            self.scan(**kwargs)
            self._add_powersupply_info()

            self._add_link_info()
            #self._add_chip_status()
            self._store_scan_par_values()

            self.logger.info('Closing raw data file: %s', self.output_filename + '.h5')
            self.h5_file.close()

            if self.socket:
                self.logger.debug('Closing socket connection')
                self.socket.close()
                self.socket = None
            
            self.analyze(**kwargs)
            
            active_column = active_column + step
            
            if finishing:
                if active_column >= 400:
                    running = False
            elif active_column + step > 400:
                finishing = True  # if 'step' is not a divisor of 400, some columns have to be configured one by one at the end (the finishing sequence)
                step = 1  # do not modify this finish step
                if active_column >= 400:
                    running = False
            
            t2 = time.time()
            print(t2-t1)

    def scan(self, **kwargs):
        '''
        Pixels test main loop

        Parameters
        ----------
        In kwargs:
            'rws' : array of int
            A list of rows to scan
            'cols' : array of int
            A list of columns to scan
        # FIXME : for now, only consecutive columns and rows can be tested. To change that, writting and reading matrix should always be full-size ones 
        '''

        self.rp = RD53ARegisterParser()

        columns = kwargs['cols']
        rows = kwargs['rws'] 

        self.logger.info('Starting scan...')
        with self.readout(fill_buffer=True, clear_buffer=True):
            
            self.logger.info('Writting {nbr_columns} columns of {rows} rows of pixels...'.format(nbr_columns=len(columns), rows=len(rows)))
            self.write_pixels(pix_conf = kwargs['pixels_config'], columns=columns, rows=rows, write=True)
            self.logger.success('Writting finished')


            ## Uncomment to allow the operator to presse ENTER after a spill/trigger
            """
            self.logger.info('Press ENTER to simulate end of spill trigger')
            input()
            """
            #time.sleep(0.5)  # simulate wait of trigger

            self.logger.info('Reading back {nbr_columns} columns of {rows} rows of pixels...'.format(nbr_columns=len(columns), rows=len(rows)))
            # Disable broadcast, enable auto-row
            indata = self.chip.write_register(register='PIX_MODE', data=0b0000000000001000, write=False)
            # Disable default configy([],
            indata += self.chip.write_register(register='PIX_DEFAULT_CONFIG', data=0, write=False)
            self.chip.write_command(indata)
           
            nb_column_really_read = 0  # usefull when some columns are configured via double column read/write because then only half data is expected back
            for column in columns:
                self.logger.info('Reading back column {column}'.format(column=column))
                pixel_index = 0 if column % 2 == 0 else 1

                if pixel_index == 0:
                    double_column = True if column + 1 in columns else False
                else:
                    double_column = False
                    if column - 1 in columns:
                        continue

                nb_column_really_read = nb_column_really_read + 1

                pair_index = 0 if column % 4 < 2 else 1
                region_in_core_column = 0 if column % 8 < 4 else 1
                region_in_core_row = rows[0] % 8
                core_column = int(column / 8)
                core_row = int(rows[0] / 8)

                # 6'b core_column + 1'b region_in_core_column + 1'b pair_index
                column_data = '0b' + str(bin(core_column))[2:].zfill(6) + str(region_in_core_column) + str(pair_index)
                row_data = '0b' + bin(core_row)[2:].zfill(6) + bin(region_in_core_row)[2:].zfill(3)

                indata = self.chip.write_register(register='REGION_ROW', data=int(row_data, 2), write=False)
                indata += self.chip.write_register(register='REGION_COL', data=int(column_data, 2), write=False)

                self.chip.write_command(indata)

                for row in rows:
                    self.chip.read_register(register=0, write=True)
                    self.chip.write_command(self.chip.write_sync(write=False) * 300)

            self.logger.info('Read operations sent. Waiting for readback data...')

            # now, raw data is pre-analyzed to verify that we get back the correct amount of values
            raw_data = self.h5_file.root.raw_data[:]
            # an error count is given : some values are read multiple times, so we expect as many more values (unique values)
            userk_data, error_count = analysis_utils.process_userk_pixels(analysis_utils.interpret_userk_data_pixels(raw_data))

            nb_pixels = len(rows) * nb_column_really_read 
            while len(userk_data) < nb_pixels+error_count:
            #repeat until correct number of values (including duplicates=errors) are returned
                raw_data = self.h5_file.root.raw_data[:]
                userk_data, error_count = analysis_utils.process_userk_pixels(analysis_utils.interpret_userk_data_pixels(raw_data))

            if DEBUG:
                for element in userk_data:
                    self.logger.info('Element of readback : {element}'.format(element=element))
                self.logger.info('Fetched {number} elements with {errors} redundancy (errors)'.format(number=len(userk_data), errors=error_count)) 


            # fetched data has to be stored in a matrix corresponding to read pixels
            j = 0
            prv_rank = -1   # a unique (when no duplicates) rank is given for each value, it is used to separate them and store in correct matrix cell
                            # is current rank is equal to previous one, this is a duplicate and the value is ignored. Previous ranks are saved every time

            test_data = np.zeros((len(rows), len(columns)))

            for column in columns:

                pixel_index = 0 if column % 2 == 0 else 1

                if pixel_index == 0:
                    double_column = True if column + 1 in columns else False
                else:
                    double_column = False
                    if column - 1 in columns:
                        continue
                
                for row in rows:
                    
                    # equivalent to a do..while loop
                    while "Fetched pixel is not next pixel":    # non-empty Strings behave as True
                                                                # while the value is a duplicate of previous one, get next value
                        # do instructions should be there

                        # break condition
                        if userk_data[j][0] != prv_rank:

                            # separation of left of right pixel value
                            if double_column:
                                mask = userk_data[j][1]
                                mask = bin(mask)[2:]
                                mask = mask.zfill(16)

                                value1_bin = '0b' + mask[8:]
                                value2_bin = '0b' + mask[:8]

                                value1 = int(value1_bin, 2)
                                value2 = int(value2_bin, 2)
                                
                                test_data[row - rows[0]][column - columns[0]] = value1
                                test_data[row - rows[0]][column + 1 - columns[0]] = value2
                            else:
                                # even when single column, the value is either a left or right one (with leading or trailing zeros)
                                mask = userk_data[j][1]
                                mask = bin(mask)[2:]
                                mask = mask.zfill(16)

                                value1_bin = '0b' + mask[8:]
                                value2_bin = '0b' + mask[:8]

                                value1 = int(value1_bin, 2)
                                value2 = int(value2_bin, 2)
                                
                                if pixel_index == 0:
                                    test_data[row - rows[0]][column - columns[0]] = value1
                                else:
                                    test_data[row - rows[0]][column - columns[0]] = value2

                            prv_rank = userk_data[j][0]
                            j = j+1
                            break
                        
                        else :
                            j = j+1
        
        self.logger.success('All read data has been fetched')

        # creation of read group of pytables and array to store the read matrix
        if not self.h5_file.__contains__("/read"):              
            read = self.h5_file.create_group(self.h5_file.root, 'read', 'Pixel read matrix')
            read_matrix_all = np.zeros((192,400))
            self.h5_file.create_array(read, 'read_matrix', obj=read_matrix_all)

        read_matrix_all = self.h5_file.root.read.read_matrix[:]  # get back the previous read matrix with values from other spills
        read_matrix_all[rows[0]:rows[-1]+1, columns[0]:columns[-1]+1] = test_data   # does not work if rows and columns are not consecutive rows and columns
                                                                                    # add read values from current spill
        
        # store back the matrix
        if self.h5_file.__contains__('/read/read_matrix'):
            self.h5_file.create_array(read, 'read_matrix_temp', obj=read_matrix_all)
            self.h5_file.root.read.read_matrix.remove()
            self.h5_file.root.read.read_matrix_temp.rename('read_matrix')
        else:
            self.h5_file.create_array(read, 'read_matrix', obj=read_matrix_all)
        
        self.logger.success('Scan finished')

    def analyze(self, **kwargs):
        self.logger.info('Comparing data...')
        results = {}

        columns = kwargs['cols']
        rows = kwargs['rws']
        
        with tb.open_file(self.output_filename + '.h5', 'r') as in_file:
        
            read_matrix_all = in_file.root.read.read_matrix[:]  # complete read matrix for all spills
            read_matrix = read_matrix_all[rows[0]:rows[-1]+1, columns[0]:columns[-1]+1]  # read matrix for current spill, regarding current columns and rows
            
            if len(read_matrix) == 0:
                raise IOError('Received no data from the chip!')
            
            written_matrix_all = in_file.root.written.written_matrix[:]  # complete written matrix for all spills
            written_matrix = written_matrix_all[rows[0]:rows[-1]+1, columns[0]:columns[-1]+1]  # written matrix for current spill, regarding current columns and rows

            if DEBUG:
                self.logger.info('Read complete matrix : {matrix}'.format(matrix=read_matrix))
                self.logger.info('Written complete matrix : {matrix}'.format(matrix=written_matrix))

            seu_count = 0
            seu_list = []
            
            dim1, dim2 = written_matrix.shape
            if len(columns) != dim2 or len(rows) != dim1:
                self.logger.error('Columns and rows to analyze do not match shape of written matrix. Terminating.')
                return False

            # finally comparing written and read values
            for column in columns:
                for row in rows:
                    
                    if(written_matrix[row - rows[0]][column - columns[0]] != read_matrix[row - rows[0]][column - columns[0]]) :
                        
                        self.logger.warning('SEU detected on pixel\tColumn {column}\tRow {row}\tWritten value : {written}\tRead value : {read}'.format(
                            column=column, row=row, written=written_matrix[row - rows[0]][column - columns[0]], read=read_matrix[row - rows[0]][column - columns[0]])) 
                        
                        seu_count = seu_count + 1
                        new_seu = ((column, row), (written_matrix[row - rows[0]][column - columns[0]], read_matrix[row - rows[0]][column - columns[0]]))
                        seu_list.append(new_seu)
                        
            if(seu_count == 0) :
                self.logger.success('No SEU detected!')
            else :
                self.logger.info('{seu_count} SEU detected.'.format(seu_count=seu_count))
       
        ### store detected SEUs in ultimate TXT file ###
        data_file = open(self.output_filename + '.txt', 'a+')

        for seu in seu_list :
            data_to_save = []
            data_to_save.append(str(seu[0][1]) + "\t" + str(seu[0][0]) + "\t" + str(seu[1][0]) + "\t" + str(seu[1][1]))
            np.savetxt(data_file,data_to_save,fmt="%s")	
        return True


    def write_pixels(self, pix_conf=np.full((192, 400), 7, dtype=int), columns=range(400), rows=range(192),  write=True):
        '''
            Apply global masks to selected columns.
            In principle, you can always write to all columns, since the information is saved in the mask, but this is very slow.
            To speed things up, you should only write the masks to the necessary columns.

            ----------
            Parameters:
                pix_conf : array of int
                    The array containing each pixel value to be written
                columns : list of int
                    A list of the columns to write the masks to
                rows : list of int
                    A list of the rows to write the masks to
        '''

        # TODO: Possible speedup using broadcast mode?
        logger = self.logger

        logger.debug('Writing masks...')
        # Disable broadcast, enable auto-row
        indata = self.chip.write_register(register='PIX_MODE', data=0b0000000000001000, write=False)
        # Disable default config
        indata += self.chip.write_register(register='PIX_DEFAULT_CONFIG', data=0, write=False)
        self.chip.write_command(indata)

        out_data = []
        
        # creation of write group of pytables and array to store the written matrix
        if not self.h5_file.__contains__("/written"):
            written = self.h5_file.create_group(self.h5_file.root, 'written', 'Pixel written matrix')
            written_matrix_all = np.zeros((192,400))
            self.h5_file.create_array(written, 'written_matrix', obj=written_matrix_all)
        
        written_matrix = np.zeros((len(rows), len(columns)))

        for column in columns:
            self.logger.info('Writing column {column}'.format(column=column))
            pixel_index = 0 if column % 2 == 0 else 1

            if pixel_index == 0:
                double_column = True if column + 1 in columns else False
            else:
                double_column = False
                if column - 1 in columns:
                    continue
                    
            pair_index = 0 if column % 4 < 2 else 1
            region_in_core_column = 0 if column % 8 < 4 else 1
            region_in_core_row = rows[0] % 8
            core_column = int(column / 8)
            core_row = int(rows[0] / 8)

            # 6'b core_column + 1'b region_in_core_column + 1'b pair_index
            column_data = '0b' + str(bin(core_column))[2:].zfill(6) + str(region_in_core_column) + str(pair_index)
            row_data = '0b' + bin(core_row)[2:].zfill(6) + bin(region_in_core_row)[2:].zfill(3)

            indata = self.chip.write_register(register='REGION_ROW', data=int(row_data, 2), write=False)
            indata += self.chip.write_register(register='REGION_COL', data=int(column_data, 2), write=False)

            for row in rows:
                # concatenation of left of right pixel value
                if double_column:
                    value1 = pix_conf[row][column]
                    value2 = pix_conf[row][column + 1]
                    value1_bin =  bin(value1)[2:].zfill(8)
                    value1_bin =  value1_bin[len(value1_bin) - 8:]
                    value2_bin =  bin(value2)[2:].zfill(8)
                    value2_bin =  value2_bin[len(value2_bin) - 8:]
                    mask = '0b' + value2_bin + value1_bin
                    
                    # both written values are stored
                    written_matrix[row - rows[0]][column - columns[0]] = int(value1_bin, 2)
                    written_matrix[row - rows[0]][column + 1 - columns[0]] = int(value2_bin, 2)
                else:
                    # even when single column, the value is either a left or right one (with leading or trailing zeros)
                    value = pix_conf[row][column]
                    value_bin =  bin(value)[2:].zfill(8)
                    value_bin =  value_bin[len(value_bin) - 8:]
                    
                    # written value is stored
                    written_matrix[row - rows[0]][column - columns[0]] = int(value_bin, 2)
                    if pixel_index == 0:
                        mask = '0b00000000' + value_bin
                    else:
                        mask = '0b' + value_bin + '00000000'

                mask = int(mask, 2)

                indata += self.chip.write_register(register=0, data=mask, write=False)
                
            if len(out_data) != 0 and (len(out_data[-1]) + len(indata)) < self.chip['cmd'].get_mem_size():
                out_data[-1] = out_data[-1] + indata
            else:
                out_data.append(indata)

        indata = []

        if write:
            for indata in out_data:
                self.chip.write_command(indata)
        
        written_matrix_all = self.h5_file.root.written.written_matrix[:]  # get back the previous written matrix with values from other spills
        written_matrix_all[rows[0]:rows[-1]+1, columns[0]:columns[-1]+1] = written_matrix   # does not work if rows and columns are not consecutive rows and columns
                                                                                            # add written values from current spill

        """
        for row in rows:
            for columns in columns:
                written_matrix_all[row][column] = written_matrix[row][column]
        # when updating this programm to allow non-consecutive rows and columns : need to use a full size written_matrix with zeros on other places and merge at the end
        """
        
        # store back the matrix
        if self.h5_file.__contains__('/written/written_matrix'):
            self.h5_file.create_array(written, 'written_matrix_temp', obj=written_matrix_all)
            self.h5_file.root.written.written_matrix.remove()
            self.h5_file.root.written.written_matrix_temp.rename('written_matrix')
        else:
            self.h5_file.create_array(written, 'written_matrix', obj=written_matrix_all)
        
        return out_data

if __name__ == '__main__':
    if len(sys.argv) > 1 and (sys.argv[1] == '-v' or sys.argv[1] == '-V' or sys.argv[1] == '-verbose' or sys.argv[1] == '--verbose'):
        DEBUG = True
    scan = PixelTest()

    scan.start(**local_configuration)
    scan.close()
